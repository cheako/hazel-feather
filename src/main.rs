// hazel-feather: A rust bassed minecraft compatible client.
// Copyright (C) 2019 Michael Mestnik

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use std::cell::{Cell, RefCell};
use std::rc::Rc;

use futures::executor::block_on;
use futures::SinkExt;
use futures::StreamExt;

use tokio::net::TcpStream;
use tokio::time::{timeout, timeout_at, Duration, Instant};
use tokio_util::codec::Framed;

use hazel_ge::{im_str, imgui, imgui::ImString};

use uuid::Uuid;

// use feather_core::bytes_ext::{BytesMutExt, TryGetError};
use feather_core::network::cast_packet;
use feather_core::network::codec::MinecraftCodec;
use feather_core::network::packet::{PacketDirection::Clientbound, PacketStage};
use feather_core::world;
use feather_core::{packet::*, PacketType};

struct ConnectionData {
    addr: RefCell<ImString>,
    nick: RefCell<ImString>,
    framed: RefCell<Framed<TcpStream, MinecraftCodec>>,
    key: [u8; 16],
    uuid: Cell<Option<Uuid>>,
}

#[allow(dead_code)]
struct GameJoinedData {
    connection_data: Rc<ConnectionData>,
    game: JoinGame,
    time: TimeUpdate,
    adjust: std::time::Instant,
    msg: ImString,
    x: f64,
    y: f64,
    z: f64,
    on_ground: bool,
    position_dirty: bool,
    yaw: f32,
    pitch: f32,
    look_dirty: bool,
    flags: u8,
    teleport_id: i32,
    locale: ImString,
    client_settings: ClientSettings,
    // #[allow(dead_code)]
    world: world::ChunkMap,
}

impl GameJoinedData {
    fn new(connection_data: Rc<ConnectionData>, game: JoinGame) -> Self {
        Self {
            connection_data,
            game,
            time: Default::default(),
            adjust: std::time::Instant::now(),
            msg: im_str!("").to_owned(),
            x: 0.,
            y: 100.,
            z: 0.,
            on_ground: true,
            position_dirty: false,
            yaw: 0.,
            pitch: 0.,
            look_dirty: false,
            flags: 0,
            teleport_id: 0,
            locale: im_str!("en_GB").to_owned(),
            client_settings: Default::default(),
            world: Default::default(),
        }
    }

    #[allow(dead_code)]
    fn get_ticks(&self) -> i64 {
        let adjust =
            (self.adjust.elapsed().as_nanos() / Duration::from_millis(20).as_nanos()) as i64;
        self.time.world_age + adjust
    }
}

#[allow(clippy::large_enum_variant)]
enum ApplicationState {
    SelectServer {
        reason: ImString,
        addr: ImString,
        nick: ImString,
    },
    ClientConnected(Rc<ConnectionData>),
    GameJoined(GameJoinedData),
}
use ApplicationState::*;

struct FeatherClient {
    application_state: ApplicationState,
}

impl Default for FeatherClient {
    fn default() -> Self {
        Self {
            application_state: SelectServer {
                reason: ImString::new("Welcome 2 Hazel-Feater!"),
                addr: ImString::new("minecraft.mikemestnik.net:25565"),
                nick: ImString::new("cheako-test"),
            },
        }
    }
}

impl imgui::Interface for FeatherClient {
    fn on_update(&mut self, _ui: &imgui::Ui) {}
}

impl hazel_ge::Layer<(), ()> for FeatherClient {
    fn get_name(&mut self) -> String {
        "Feather Client".to_owned()
    }

    fn on_imgui_update(&mut self, ui: &hazel_ge::imgui::Ui) {
        let next_action_state = match &mut self.application_state {
            SelectServer { reason, addr, nick } => {
                if let Some(w) =
                    hazel_ge::reexport::imgui::Window::new(im_str!("Welcome Screen")).begin(&ui)
                {
                    ui.text(reason);
                    ui.input_text(im_str!("Addr"), addr).build();
                    ui.input_text(im_str!("Nick"), nick).build();
                    let ret = if ui.button(im_str!("Connect"), [120f32, 30.]) {
                        let stream = block_on(TcpStream::connect(addr.to_str())).unwrap();

                        let codec = MinecraftCodec::new(Clientbound);

                        let mut framed = Framed::new(stream, codec);

                        block_on(framed.send(Box::new(Handshake {
                            protocol_version: 404,
                            server_address: addr.to_string(),
                            server_port: 25565,
                            next_state: HandshakeState::Login,
                        })))
                        .unwrap();
                        framed.codec_mut().set_stage(PacketStage::Login);
                        block_on(framed.send(Box::new(LoginStart {
                            username: nick.to_string(),
                        })))
                        .unwrap();

                        Some(ClientConnected(Rc::new(ConnectionData {
                            addr: RefCell::new(addr.clone()),
                            nick: RefCell::new(nick.clone()),
                            framed: RefCell::new(framed),
                            key: rand::random(),
                            uuid: Default::default(),
                        })))
                    } else {
                        None
                    };
                    w.end(&ui);
                    ret
                } else {
                    None
                }
            }
            GameJoined(GameJoinedData {
                connection_data,
                msg,
                locale,
                client_settings,
                ..
            }) => {
                let mut framed = connection_data.framed.borrow_mut();
                if let Some(w) =
                    hazel_ge::reexport::imgui::Window::new(im_str!("Change Status")).begin(&ui)
                {
                    if ui.button(im_str!("Respawn"), [120f32, 30.]) {
                        block_on(framed.send(Box::new(ClientStatus { action_id: 0 }))).unwrap();
                    }
                    if ui.button(im_str!("Stats"), [120f32, 30.]) {
                        block_on(framed.send(Box::new(ClientStatus { action_id: 1 }))).unwrap();
                    }
                    w.end(&ui);
                }
                if let Some(w) = hazel_ge::reexport::imgui::Window::new(im_str!("Chat")).begin(&ui)
                {
                    ui.input_text(im_str!("Msg"), msg).build();
                    if ui.button(im_str!("Send"), [80f32, 30.]) {
                        block_on(framed.send(Box::new(ChatMessageServerbound {
                            message: msg.to_owned().to_string(),
                        })))
                        .unwrap();
                        msg.clear();
                    }
                    w.end(&ui);
                }
                if let Some(w) =
                    hazel_ge::reexport::imgui::Window::new(im_str!("Client Settings")).begin(&ui)
                {
                    ui.input_text(im_str!("Locale"), locale).build();
                    hazel_ge::reexport::imgui::Slider::new(
                        im_str!("View Distance"),
                        std::ops::RangeInclusive::new(0, 255),
                    )
                    .build(&ui, &mut client_settings.view_distance);
                    ui.radio_button(im_str!("Chat Enabled"), &mut client_settings.chat_mode, 0);
                    ui.radio_button(
                        im_str!("Chat Commands Only"),
                        &mut client_settings.chat_mode,
                        1,
                    );
                    ui.radio_button(im_str!("Chat Hidden"), &mut client_settings.chat_mode, 2);
                    if ui.radio_button_bool(im_str!("Chat Colors"), client_settings.chat_colors) {
                        client_settings.chat_colors = !client_settings.chat_colors;
                    }
                    let ds = &mut client_settings.displayed_skin_parts;
                    if ui.radio_button_bool(im_str!("Cape enabled"), *ds & 1 != 0) {
                        *ds ^= 1;
                    }
                    if ui.radio_button_bool(im_str!("Jacket enabled"), *ds & 2 != 0) {
                        *ds ^= 2;
                    }
                    if ui.radio_button_bool(im_str!("Left Sleeve enabled"), *ds & 4 != 0) {
                        *ds ^= 4;
                    }
                    if ui.radio_button_bool(im_str!("Right Sleeve enabled"), *ds & 8 != 0) {
                        *ds ^= 8;
                    }
                    if ui.radio_button_bool(im_str!("Left Pants Leg enabled"), *ds & 0x10 != 0) {
                        *ds ^= 0x10;
                    }
                    if ui.radio_button_bool(im_str!("Right Pants Leg enabled"), *ds & 0x20 != 0) {
                        *ds ^= 0x20;
                    }
                    if ui.radio_button_bool(im_str!("Hat enabled"), *ds & 0x40 != 0) {
                        *ds ^= 0x40;
                    }
                    ui.radio_button(im_str!("Left Handed"), &mut client_settings.main_hand, 0);
                    ui.radio_button(im_str!("Right Handed"), &mut client_settings.main_hand, 1);
                    if ui.button(im_str!("Submit"), [80f32, 30.]) {
                        client_settings.locale = locale.to_string();
                        block_on(framed.send(Box::new(client_settings.clone()))).unwrap();
                    }
                    w.end(&ui);
                }
                None
            }
            _ => None,
        };
        if let Some(application_state) = next_action_state {
            self.application_state = application_state;
        }
    }

    fn on_update(
        &mut self,
        api: &mut hazel_ge::AppAPI<(), ()>,
        _w: &hazel_ge::EventLoopWindowTarget<()>,
        _c: &mut hazel_ge::ControlFlow,
    ) {
        let mut next_application_state: Option<ApplicationState> = None;
        match &mut self.application_state {
            ClientConnected(connection_data) => {
                let mut framed = connection_data.framed.borrow_mut();
                while let Ok(Some(Ok(packet))) =
                    block_on(timeout(Duration::from_millis(10), framed.next()))
                {
                    match packet.ty() {
                        PacketType::EncryptionRequest => {
                            let y = cast_packet::<EncryptionRequest>(packet);
                            use rsa::PublicKey;

                            let (n, e) = rsa_der::public_key_from_der(&y.public_key).unwrap();
                            let rsakey = rsa::RSAPublicKey::new(
                                num_bigint::BigUint::from_bytes_be(&n),
                                num_bigint::BigUint::from_bytes_be(&e),
                            )
                            .unwrap();

                            let mut rng = rand::rngs::OsRng::new().unwrap();

                            block_on(
                                framed.send(Box::new(EncryptionResponse {
                                    secret: rsakey
                                        .encrypt(
                                            &mut rng,
                                            rsa::PaddingScheme::PKCS1v15,
                                            &connection_data.key,
                                        )
                                        .unwrap(),
                                    verify_token: rsakey
                                        .encrypt(
                                            &mut rng,
                                            rsa::PaddingScheme::PKCS1v15,
                                            &y.verify_token,
                                        )
                                        .unwrap(),
                                })),
                            )
                            .unwrap();
                        }
                        PacketType::DisconnectLogin => {
                            let y = cast_packet::<DisconnectLogin>(packet);
                            next_application_state = Some(SelectServer {
                                reason: ImString::new(y.reason.clone()),
                                addr: connection_data.addr.borrow().clone(),
                                nick: connection_data.nick.borrow().clone(),
                            });
                            break;
                        }
                        PacketType::SetCompression => {
                            use std::convert::TryInto;
                            let y = cast_packet::<SetCompression>(packet);
                            framed
                                .codec_mut()
                                .enable_compression(y.threshold.try_into().unwrap());
                        }
                        PacketType::LoginSuccess => {
                            let y = cast_packet::<LoginSuccess>(packet);
                            let nick = connection_data.nick.borrow();
                            if y.username != nick.to_string() {
                                next_application_state = Some(SelectServer {
                                    reason: ImString::new(format!(
                                        "Username not match: {}",
                                        y.username
                                    )),
                                    addr: connection_data.addr.borrow().clone(),
                                    nick: nick.clone(),
                                });
                                break;
                            } else if let Ok(_uuid) = Uuid::parse_str(&y.uuid) {
                                connection_data.uuid.set(Some(_uuid));
                                framed.codec_mut().set_stage(PacketStage::Play);
                            } else {
                                next_application_state = Some(SelectServer {
                                    reason: ImString::new(format!("Not a uuid: {}", y.uuid)),
                                    addr: connection_data.addr.borrow().clone(),
                                    nick: nick.clone(),
                                });
                                break;
                            };
                        }
                        PacketType::JoinGame => {
                            let y = cast_packet::<JoinGame>(packet);
                            println!("{:?}", y);
                            next_application_state = Some(GameJoined(GameJoinedData::new(
                                connection_data.clone(),
                                y.clone(),
                            )));
                            break;
                        }
                        PacketType::KeepAliveClientbound => {
                            let y = cast_packet::<KeepAliveClientbound>(packet);
                            block_on(framed.send(Box::new(KeepAliveServerbound {
                                id: y.keep_alive_id,
                            })))
                            .unwrap();
                        }
                        _ => unimplemented!(),
                    }
                }
            }
            #[allow(unused_variables)]
            GameJoined(GameJoinedData {
                connection_data,
                game,
                time,
                adjust: time_adjust,
                x: player_x,
                y: player_y,
                z: player_z,
                on_ground: player_on_ground,
                position_dirty: player_position_dirty,
                yaw: player_yaw,
                pitch: player_pitch,
                look_dirty: player_look_dirty,
                flags: player_flags,
                teleport_id: player_teleport_id,
                world,
                ..
            }) => {
                let mut framed = connection_data.framed.borrow_mut();
                while let Ok(Some(Ok(packet))) = block_on(timeout_at(
                    Instant::from(api.ts) + Duration::from_nanos(4_166_667),
                    framed.next(),
                )) {
                    let x = packet.ty();
                    if x != PacketType::KeepAliveClientbound
                        && x != PacketType::ChunkData
                        && x != PacketType::SetSlot
                    {
                        println!("{:?}", x)
                    };
                    match x {
                        PacketType::ChunkData => {
                            let y = cast_packet::<ChunkData>(packet);
                            world.insert(y.chunk.read().clone());
                        }
                        PacketType::KeepAliveClientbound => {
                            let y = cast_packet::<KeepAliveClientbound>(packet);
                            block_on(framed.send(Box::new(KeepAliveServerbound {
                                id: y.keep_alive_id,
                            })))
                            .unwrap();
                        }
                        PacketType::EntityMetadata => {
                            let y = cast_packet::<PacketEntityMetadata>(packet);
                            println!("{:?}", y);
                        }
                        PacketType::SpawnPosition => {
                            let y = cast_packet::<SpawnPosition>(packet);
                            *player_x = y.location.x as _;
                            *player_y = y.location.y as _;
                            *player_z = y.location.z as _;
                        }
                        PacketType::PlayerPositionAndLookClientbound => {
                            let y = cast_packet::<PlayerPositionAndLookClientbound>(packet);
                            *player_x = y.x;
                            *player_y = y.y;
                            *player_z = y.z;
                            *player_yaw = y.yaw;
                            *player_pitch = y.pitch;
                            *player_flags = y.flags;
                            *player_teleport_id = y.teleport_id;
                        }
                        PacketType::PlayerInfo => {
                            let y = cast_packet::<PlayerInfo>(packet);
                            println!("{:?}", y);
                        }
                        PacketType::SetSlot => {
                            let y = cast_packet::<SetSlot>(packet);
                            println!("{:?}", y);
                        }
                        PacketType::ChatMessageClientbound => {
                            let y = cast_packet::<ChatMessageClientbound>(packet);
                            println!("{:?}", y);
                        }
                        PacketType::TimeUpdate => {
                            let y = cast_packet::<TimeUpdate>(packet);
                            *time_adjust = std::time::Instant::now();
                            *time = y.clone();
                            println!("{:?}", y);
                        }
                        _ => unimplemented!(),
                    }
                }
                let elapsed = api.ts.elapsed();
                use hazel_ge::event::VirtualKeyCode as Key;
                let key_a = api.input.get_key(Key::A);
                let key_d = api.input.get_key(Key::D);
                let key_r = api.input.get_key(Key::R);
                let key_v = api.input.get_key(Key::V);
                let key_w = api.input.get_key(Key::W);
                let key_s = api.input.get_key(Key::S);
                match (key_a ^ key_d, key_a) {
                    (true, true) => {
                        *player_yaw += elapsed.as_nanos() as f32;
                        *player_look_dirty = true;
                    }
                    (true, false) => {
                        *player_yaw -= elapsed.as_nanos() as f32;
                        *player_look_dirty = true;
                    }
                    _ => {}
                }
                match (key_a ^ key_d, key_a) {
                    (true, true) => {
                        *player_x +=
                            (*player_yaw as f64).to_radians().cos() * elapsed.as_nanos() as f64;
                        *player_z +=
                            (*player_yaw as f64).to_radians().sin() * elapsed.as_nanos() as f64;
                        *player_position_dirty = true;
                    }
                    (true, false) => {
                        *player_x -=
                            (*player_yaw as f64).to_radians().cos() * elapsed.as_nanos() as f64;
                        *player_z -=
                            (*player_yaw as f64).to_radians().sin() * elapsed.as_nanos() as f64;
                        *player_position_dirty = true;
                    }
                    _ => {}
                }
                match (key_r ^ key_v, key_r) {
                    (true, true) => {
                        *player_x +=
                            (*player_yaw as f64).to_radians().sin() * elapsed.as_nanos() as f64;
                        *player_z +=
                            (*player_yaw as f64).to_radians().cos() * elapsed.as_nanos() as f64;
                        *player_position_dirty = true;
                    }
                    (true, false) => {
                        *player_x -=
                            (*player_yaw as f64).to_radians().sin() * elapsed.as_nanos() as f64;
                        *player_z -=
                            (*player_yaw as f64).to_radians().cos() * elapsed.as_nanos() as f64;
                        *player_position_dirty = true;
                    }
                    _ => {}
                }
                match (*player_position_dirty, *player_look_dirty) {
                    (true, true) => {
                        block_on(framed.send(Box::new(PlayerPositionAndLookServerbound {
                            x: *player_x,
                            feet_y: *player_y,
                            z: *player_z,
                            yaw: *player_yaw,
                            pitch: *player_pitch,
                            on_ground: *player_on_ground,
                        })))
                        .unwrap();
                        *player_position_dirty = false;
                        *player_look_dirty = false;
                    }
                    (true, false) => {
                        block_on(framed.send(Box::new(PlayerPosition {
                            x: *player_x,
                            feet_y: *player_y,
                            z: *player_z,
                            on_ground: *player_on_ground,
                        })))
                        .unwrap();
                        *player_position_dirty = false;
                    }
                    (false, true) => {
                        block_on(framed.send(Box::new(PlayerLook {
                            yaw: *player_yaw,
                            pitch: *player_pitch,
                            on_ground: *player_on_ground,
                        })))
                        .unwrap();
                        *player_look_dirty = false;
                    }
                    (false, false) => {}
                }
            }
            _ => {}
        }
        if let Some(application_state) = next_application_state {
            self.application_state = application_state;
        }
    }
}

#[tokio::main]
async fn main() {
    let mut app: hazel_ge::Application<(), ()> = Default::default();
    let feather = Rc::new(RefCell::new(FeatherClient::default()));
    app.layers.push(vec![feather.clone()]);
    app.layers
        .push(vec![Rc::new(RefCell::new(imgui::InitLayer::new(
            feather, 1,
        )))]);
    app.run()
}
