#!/bin/bash

perl -0 -ne 'chomp; m!^([0-9]+)/([0-9]+)/(.*)!s or next; utime $1, $2, $3;' .cache/dates.dat
mkdir -p .cache && {
    : ${RUSTUP_HOME:=${HOME}/.rustup} ${CARGO_HOME:=${HOME}/.cargo}

    [ "$(type -p -t rsync)" == "file" ] || apt-get -y -qq update && apt-get -y -qq --no-install-suggests --no-install-recommends install rsync
    rsync -atu --no-whole-file --inplace "${RUSTUP_HOME}/." .cache/.rustup && mv -f "$RUSTUP_HOME" "$(dirname "$RUSTUP_HOME")/old_$(basename "$RUSTUP_HOME")" && ln -s "$(pwd)"/.cache/.rustup "$RUSTUP_HOME" &
    rsync -atu --no-whole-file --inplace "${CARGO_HOME}/." .cache/.cargo && mv -f "$CARGO_HOME" "$(dirname "$CARGO_HOME")/old_$(basename "$CARGO_HOME")" && ln -s "$(pwd)"/.cache/.cargo "$CARGO_HOME"
    wait
}
